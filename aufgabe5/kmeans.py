#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
# Now import the utilities from task 1.  Make sure the file imgutils.py is
# in the same folder as this file.
import imgutils


def compute_means(clusters):
    res = []
    for cluster in clusters:
        res.append(imgutils.color_average(cluster))
    return res

# Du willst die euklidische Distanz eines Pixelwertes bestimmen zu einem Mittelwert und dann mit den
# euklidischen Distanzen dieses Pixels zu allen anderen Mittelwerten vergleichen.
# Wenn die Distanz kleiner im Vergleich zu allen anderen Mittelwerten ist dann gehört
# der Pixel in das Cluster von dem Mittelwert.
# x_j sind die RGB Werte

def compute_clusters(pixels, means):
    n = len(means)
    clusters = [[] for i in range(n)]
    for pixel in pixels:
        distances = []
        for tuple in means:
            distances.append(compute_eucledian_distance(tuple, pixel))
        clusters[distances.index(min(distances))].append(pixel)
    return clusters

def compute_eucledian_distance(valOne, valTwo):
    a = np.array((valOne[0], valOne[1], valOne[2]))
    b = np.array((valTwo[0], valTwo[1], valTwo[2]))
    return np.linalg.norm(a-b)


def averaged_pixels(clusters, means):
    assert(len(clusters) == len(means))
    res = []
    index = 0
    for cluster in clusters:
        for pixel in cluster:
            tup = (means[index][0], means[index][1], means[index][2], pixel[3], pixel[4])
            res.append(tup)
        index = index+1
    return res


def kmeans(image, k):
    pixels = imgutils.image2pixels(image)
    pixels = pixel_kmeans(pixels, k)
    return imgutils.pixels2image(pixels)


def pixel_kmeans(pixels, k):
    # Ensure that k is never bigger than the number of pixels.
    k = min(k, len(pixels))
    # Choose some initial clusters.  A better strategy would improve the
    # algorithm quite a bit, but this simple technique is usually fine, too.
    clusters = [[pixels[i]] for i in range(0, len(pixels)-1, len(pixels) // k)]
    means = compute_means(clusters)
    while True:
        assert(len(means) == k)
        assert(len(clusters) == k)
        clusters = compute_clusters(pixels, means)
        new_means = compute_means(clusters)
        if means == new_means:
            return averaged_pixels(clusters, means)
        means = new_means


def image_difference(imageA, imageB):
    """Compute the difference of two images in the maximum-norm."""
    if imageA.shape != imageB.shape:
        raise ValueError('Dimensions of both images must be equal!')
    diff = np.array(imageA,dtype='int16') - np.array(imageB,dtype='int16')
    return np.amax(abs(diff))


def main():
    shibuya = imgutils.load_image('shibuya_small.png')
    k = 4
    shibuya2 = kmeans(shibuya, k)
    plt.imshow(shibuya2)
    plt.show()
    cluster = [[(0, 0, 0, 0, 0)], [(1, 0, 0, 0, 0)]]
    # compute_clusters([(0, 0, 0, 0, 0)], [(0, 0, 0)])
    averaged_pixels([[(0, 0, 253, 0, 2), (0, 0, 254, 1, 2), (0, 0, 255, 2, 2)],
                     [(0, 253, 0, 0, 1), (0, 254, 0, 1, 1), (0, 255, 0, 2, 1)],
                     [(14, 0, 0, 0, 0), (13, 0, 0, 1, 0), (12, 0, 0, 2, 0)]],
                 [(0, 0, 254), (0, 254, 0), (13, 0, 0)])

if __name__ == "__main__": main()