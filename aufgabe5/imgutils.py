#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt


def load_image(path):
    """Load the specified image as uint8 Numpy array."""
    return np.uint8(plt.imread(path) * 255)


def image2pixels(image):
    """Create a list of (R, G, B, X, Y) tuples from a given 3d image array."""
    result = []
    for y in range(0, len(image)):
        for x in range(0, len(image[0])):
            tup1 = []
            for z in range(0, 3):
                tup1.append(image[y][x][z])
                if z == 2:
                    tup1.append(x)
                    tup1.append(y)
            result.append(tuple(tup1))

    return result


def pixels2image(pixels):
    """Create a 3d image array from a list of (R, G, B, X, Y) pixels."""
    x = max(pixels, key=lambda item:item[3])[3]
    # x1 = max(pixels,key=itemgetter(1))
    y = max(pixels, key=lambda item: item[4])[4]
    nparray = np.zeros(shape=(y+1, x+1, 3))
    for item in pixels:
        for index in range(0,3):
            nparray[item[4]][item[3]][index] = item[index]

    return np.uint8(nparray)

def bounding_box(pixels):
    """Return a tuple ((Rmin, Rmax), (Gmin, Gmax), (Bmin, Bmax)) with the
    respective minimum and maximum values of each color."""
    rMax = max(pixels, key=lambda item:item[0])[0]
    rMin = min(pixels, key=lambda item: item[0])[0]
    gMax = max(pixels, key=lambda item: item[1])[1]
    gMin = min(pixels, key=lambda item: item[1])[1]
    bMax = max(pixels, key=lambda item: item[2])[2]
    bMin = min(pixels, key=lambda item: item[2])[2]
    return ((rMin, rMax),(gMin,gMax),(bMin,bMax))


def color_average(pixels):
    """Return list of tuples (Ravg, Gavg, Bavg) with averaged color values."""
    rGes = 0
    gGes = 0
    bGes = 0
    countItems = len(pixels)
    for item in pixels:
        rGes = rGes + item[0]
        gGes = gGes + item[1]
        bGes = bGes + item[2]

    return (round(rGes/countItems),round(gGes/countItems),round(bGes/countItems))


def main():

    # array = [(1, 2, 3, 0, 0), (4, 5, 6, 0, 1)]
    # pixels2image(array)
    shibuya = load_image('shibuya.png')
    pixels = image2pixels(shibuya)
    print(bounding_box(pixels))
    print(color_average(pixels))
    plt.imshow(pixels2image(pixels))
    plt.show()


if __name__ == "__main__": main()