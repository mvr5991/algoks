#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
# Now import the utilities from task 1.  Make sure the file imgutils.py is
# in the same folder as this file.
import imgutils



def cut_dimension(bbox):
    cutR = bbox[0][1] - bbox[0][0]
    cutG = bbox[1][1] - bbox[1][0]
    cutB = bbox[2][1] - bbox[2][0]
    if (cutR >= cutG) and (cutR >= cutB):
        max = 0
    elif (cutG >= cutR) and (cutG >= cutB):
        max = 1
    else:
        max = 2
    return max


def recursive_median_cut(pixels, N, bbox=False):
    if len(pixels) < 2:
        return pixels
    if N == 0:
        (ravg, gavg, bavg) = imgutils.color_average(pixels)
        return [(ravg, gavg, bavg, pixel[3], pixel[4]) for pixel in pixels]
    if bbox == False:
        bbox = imgutils.bounding_box(pixels)
    index = cut_dimension(bbox)
    pixels.sort(key = lambda item: item[index])
    middle = int(np.floor(len(pixels)/2))
    N = N-1
    left = pixels[0:middle]
    right = pixels[middle:len(pixels)]
    leftRec= recursive_median_cut(left, N, False)
    rightRec = recursive_median_cut(right, N, False)
    leftRec.extend(rightRec)
    return leftRec


def median_cut(image, ncuts=8):
    """Perform the median cut algorithm on a given image."""
    pixels = imgutils.image2pixels(image)
    pixels = recursive_median_cut(pixels, ncuts)
    return imgutils.pixels2image(pixels)


def image_difference(imageA, imageB):
    """Compute the difference of two images in the maximum-norm."""
    if imageA.shape != imageB.shape:
        raise ValueError('Dimensions of both images must be equal!')
    diff = np.array(imageA,dtype='int16') - np.array(imageB,dtype='int16')
    return np.amax(abs(diff))


def main():
    # recursive_median_cut([[0, 0, 0],[0, 0, 0]],[[2, 2, 2],[2, 3, 2]],2)
    # print(recursive_median_cut([(0, 0, 0, 0, 0), (1, 1, 1, 1, 0), (2, 2, 2, 2, 0), (3, 3, 3, 3, 0)],1,False))
    cut_dimension(((1, 1), (2, 3), (3, 4)))
    shibuya = imgutils.load_image('shibuya.png')
    lena    = imgutils.load_image('lena.png')
    # median cut quantisation with 2^N colors
    N = 5
    shibuya2 = median_cut(shibuya, N)
    lena2 = median_cut(lena, N)
    print("shibuya 2^{} colors: {}".format(N, image_difference(shibuya, shibuya2)))
    print("lena    2^{} colors: {}".format(N, image_difference(lena, lena2)))
    plt.imshow(lena2)
    plt.show()
    plt.imshow(shibuya2)
    plt.show()


if __name__ == "__main__": main()
