#!/usr/bin/env python3


from math import *
import numpy as np

def rotation_matrix(omega):
    return np.array([[cos(omega),-sin(omega)],[sin(omega),cos(omega)]])


def stretch_matrix(s, d):
    array = np.zeros(shape=(d,d))
    for x in range(0,d):
        for y in range(0,d):
            if x == y:
                array[x][y] = 1*s
            else:
                array[x][y] = 0

    return array


def compose(*matrices):
    if not matrices:
        raise Exception("Expected at least one Matrix")
    return functools.reduce(np.dot, matrices)


def main():
    # Hier kann beliebiger Testcode stehen, der bei der Korrektur ignoriert wird
    pass


if __name__ == "__main__": main()