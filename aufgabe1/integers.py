#!/usr/bin/env python3

from math import *

def is_prime(n):
    if n == 0 or n == 1:
        return False
    for i in range(2, n//2+1):
        if n % i == 0:
            return False
    return True




def int2str(n, base):
    digits = "0123456789abcdefghijklmnopqrstuvwxyz"
    if not 2 <= base <= 36: raise ValueError("Invalid base")
    if n == 0: return '0'
    result = n
    restKette = ''
    while result > 0:
        restKette = digits[result%base] + restKette
        result = result//base
    return restKette


def is_emirp(n):
    if not is_prime(n): return False
    reverse = str(n)[::-1]
    if int(reverse) == n: return False
    return is_prime(int(reverse))

def main():
    print(int2str(51966, 16))
    # Beispiel:
    # for n in [1, 2, 3, 4, 5, 6, 7, 97, 98, 99]:
    #     print("Ist " + str(n) + " eine Primzahl? " + str(is_prime(n)))
    #
    #
    # # Beispiel:
    # print("Mirpzahlen: ", end='')
    # for n in range(99):
    #     if is_emirp(n):
    #         print(' ' + str(n), end='')


if __name__ == "__main__": main()