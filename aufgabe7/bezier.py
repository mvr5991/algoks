#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

# P = np.array([[3., 2.], [2., 5.], [7., 6.], [8., 1.]])

def de_casteljau_step(P, t):
    """For a given control polygon P of length n, return a control polygon of
    length n-1 by performing a single de Casteljau step with the given
    floating point number t."""
    newLen = len(P) - 1
    newP = np.empty(shape=(newLen, len(P[0])))
    for k in range(0,newLen):
        for z in range(0, len(P[0])):
            newP[k][z] = (1-t) * P[k][z] + t * P[k+1][z]

    return newP



def de_casteljau(P, t):
    """Evaluate the Bezier curve specified by the control polygon P at a single
    point corresponding to the given t in [0,1]. Returns a one-dimensional
    NumPy array contining the x and y coordinate of the Point,
    respectively."""
    assert len(P) != 0
    res = P
    for x in range(0, len(P)-1):
        res = de_casteljau_step(res, t)
    return np.ndarray.flatten(res)


def bezier1(P, m):
    """Return a polygon with m points that approximates the Bezier curve
    specified by the control polygon P."""
    assert len(P) > 1, m > 1
    start = 0
    stepsize = 1/(m-1)
    res = np.empty(shape=(m, len(P[0])))
    for x in range(0, m):
        ret = de_casteljau(P, start)
        for z in range(0, len(P[0])):
            res[x][z] = ret[z]
        start = start + stepsize
    return res


def add_control_point(P):
    """For the given Bezier curve control polygon P of length n, return a new
    control polygon with n+1 points that describes the same curve."""
    assert len(P) > 1
    Q = np.empty(shape=(len(P)+1, len(P[0])))
    Q[0] = P[0]
    Q[len(Q)-1] = P[len(P)-1]
    for i in range(1, len(Q)-1):
        alpha = i/len(P)
        Q[i] = alpha * P[i-1] + (1-alpha) * P[i]
    return Q


def split_curve(P):
    #rekursion
    left, right = []
    #calc bezier
    newP = de_casteljau(P, 0.5)
    # n = len(P)
    # copyP = P
    # while len(copyP) < (2*n)-1:
    #     copyP = add_control_point(copyP)
    # middle = len(copyP)//2
    # left = copyP[0:middle+1]
    # right = copyP[middle:len(copyP)]
    #
    #
    #
    # return (left, right)
    # while len(left) < n or len(right) < n:
    #     if len(left)< n:
    #         left = add_control_point(left)
    #     if len(right)< n:
    #         right = add_control_point(right)
    # return (left, right)


def bezier2(P, depth):
    """Return a polygon that approximates the Bezier curve specified by the
    control polygon P by depth recursive subdivisions."""
    pass # TODO


def de_casteljau_plot(P):
    """Draw all polygons in the de Casteljau pyramid of P for varying t."""
    n = len(P)
    t = 0.3
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    lines = ax.plot(P[:,0], P[:,1], 'o-')
    Q = P.copy()
    for i in range(n-1):
        Q = de_casteljau_step(Q,t)
        [line] = ax.plot(Q[:,0], Q[:,1], 'o-')
        lines.append(line)
    plt.grid(True)

    def redraw(t):
        Q = P.copy()
        for i in range(n-1):
            Q = de_casteljau_step(Q,t)
            lines[i+1].set_xdata(Q[:,0])
            lines[i+1].set_ydata(Q[:,1])

    fig.subplots_adjust(left=0.25, bottom=0.25)
    fig.canvas.draw_idle()
    t_slider_ax  = fig.add_axes([0.25, 0.1, 0.5, 0.03])
    t_slider = Slider(t_slider_ax, 't', 0., 1., valinit=t)
    t_slider.on_changed(redraw)
    plt.show()


def bezier_plot(P):
    """Draw different bezier curve approximations for the given P."""
    n = len(P)
    depth = 1
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    B2 = bezier2(P.copy(), depth)
    B1 = bezier1(P.copy(), len(B2))
    [line0] = ax.plot( P[:,0],  P[:,1], 'o-', label="P")
    [line1] = ax.plot(B1[:,0], B1[:,1], 'o-', label="bezier1")
    [line2] = ax.plot(B2[:,0], B2[:,1], 'o-', label="bezier2")
    plt.legend(shadow=True)
    plt.grid(True)

    def redraw(depth):
        depth = int(depth)
        B2 = bezier2(P.copy(), depth)
        line2.set_xdata(B2[:,0])
        line2.set_ydata(B2[:,1])
        B1 = bezier1(P.copy(), len(B2))
        line1.set_xdata(B1[:,0])
        line1.set_ydata(B1[:,1])

    fig.subplots_adjust(left=0.25, bottom=0.25)
    fig.canvas.draw_idle()
    depth_slider_ax  = fig.add_axes([0.25, 0.1, 0.5, 0.03])
    depth_slider = Slider(depth_slider_ax, 'depth', 0, 7, valinit=depth)
    depth_slider.on_changed(redraw)
    plt.show()


def main():
    # P = np.array([[3., 2.], [2., 5.], [7., 6.], [8., 1.]])
    # de_casteljau_plot(P)
    # bezier_plot(P)
    # P = np.array([[0., 0.],[1., 0.]])
    # de_casteljau_step(P, 0.5)
    P = np.array([[0., 0.],[1., 1.],[2., 0.]])
    # bezier1(P, 2)
    Q = add_control_point(P)
    Q = add_control_point(Q)
    # Q = add_control_point(Q)
    print(Q)
    # bezier_plot(P)
    # bezier_plot(Q)


if __name__ == "__main__": main()