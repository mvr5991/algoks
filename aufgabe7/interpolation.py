#!/usr/bin/env python3

import numpy as np
import numpy.linalg
import matplotlib.pyplot as plt


def interpolate_linearly(a, b):
    """Return an object of type numpy.poly1d withe degree 1 that passes through a and b."""
    z = np.polyfit(np.array(a), np.array(b), 1)
    return np.poly1d(z)


def newton_matrix(X):
    """Setup the matrix of the LSE which is used to determine the coefficients
    of the Newton-basis.  X are the x-coordinates of the nodes which are
    used for interpolation."""
    pass # TODO


def newton_polynomial(C, X):
    """Take coefficients and interpolation point x-coordinates of the
Newton-polynomial and determine the corresponding interpolation polynomial."""
    assert len(C) == len(X)
    pass # TODO


def interpolating_polynomial(X,Y):
    """Determine the interpolating polynomial for the given NumPy arrays of x and y coordinates."""
    assert len(X) == len(Y)
    pass # TODO


def interpolation_plot(X,Y):
    p = interpolating_polynomial(X, Y)
    px = np.arange(min(X)-0.1, max(X)+0.11, 0.01)
    plt.grid(True)
    plt.plot(X, Y, "o")
    plt.plot(px, p(px))
    plt.show()


def main():
    X = np.array([0, 1, 2, 3])
    Y = np.array([-2.,3.,1.,2.])
    a = np.array([0.0, 0.0])
    b = np.array([1.0, 0.0])
    interpolate_linearly(a, b)

if __name__ == "__main__": main()
