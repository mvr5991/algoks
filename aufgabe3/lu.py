#!/usr/bin/env python3

import numpy as np

def swap_rows(A, row1, row2):
    A[[row1, row2]] = A[[row2, row1]]
    return A


def subtract_scaled(A, dst, src, scale):
    """Subtract scale times the row src from the row dst of A"""
    A[[dst]] = A[[dst]] - scale * A[[src]]
    return A


def pivot_index(A, column):
    """Compute the row index of the maximum element of A in this column,
       excluding all elements above the diagonal of A."""
    highest = 0
    row = column
    for x in range(column, A.shape[0]):
        if highest < np.absolute(A[x][column]):
            highest = np.absolute(A[x][column])
            row = x

    return row


def getIdMatrix(d):
    array = np.zeros(shape=(d, d))
    for x in range(0, d):
        for y in range(0, d):
            if x == y:
                array[x][y] = 1
            else:
                array[x][y] = 0

    return array


def lu_decompose(A):
    pList = []
    length = len(A)
    L = getIdMatrix(length)
    R = A
    P = getIdMatrix(length)
    for k in range (0, length-1):
        P = getIdMatrix(length)
        i = pivot_index(R, k)
        for q in range(k,length):
            tmp = R[k][q]
            R[k][q] = R[i][q]
            R[i][q] = tmp
        for z in range(0, k):
            tmp = L[k][z]
            L[k][z] = L[i][z]
            L[i][z] = tmp
        P = swap_rows(P, k, i)
        pList.append(P)
        for j in range(k+1, length):
            print(R[k][k])
            L[j][k] = (float)(R[j][k])/(R[k][k])
            for y in range(k,length):
                R[j][y] = R[j][y] - (L[j][k] * R[k][y])
    if len(pList) > 0:
        P = multiplyListOfMatrices(pList)
    return (P,L,R)


def multiplyListOfMatrices(list):
    if len(list) > 0:
        result = list.pop(0)
        for ai in list:
            result = np.matmul(result, ai)
        return result
    return []


def forward_substitute(L, b):
    """Return y such that L @ y = b"""
    x = []
    n = len(L)
    for j in range(0, n):
        x.append(b[j])
        for i in range(0, j):
            x[j] = x[j] - (L[j, i] * x[i])
        x[j] = x[j] / L[j, j]
    return np.array(x)


def backward_substitute(R, y):
    length = len(y)
    x = np.array(y)
    for j in range(length-1,-1, -1):
        x[j] = y[j]
        for i in range(j+1, length):
            x[j] -= (R[j, i] * x[i])
        x[j] = x[j] / R[j, j]
    return x


def linsolve(A, *bs):
    """Return (x1, ..., xn), such that A @ xk = bs[k]"""
    P,L,U = lu_decompose(A)
    res = []
    for b in bs:
        y = forward_substitute(L,b)
        backward = backward_substitute(U,y)
        res.append(backward)
    return tuple(res)


def main():

    if __name__ == "__main__": main()
