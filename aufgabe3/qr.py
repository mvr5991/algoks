#!/usr/bin/env python3

import numpy as np
import math


def givens_rotation2(A, i, j):
    J = np.copy(A)
    int(np.sign(A[j][j]))
    c = (np.sign(A[j][j]) * A[j][j])/math.sqrt(A[j][j]**2+A[i][j]**2)
    s = (-1*np.sign(A[j][j])* A[i][j])/math.sqrt(A[j][j]**2+A[i][j]**2)
    for q in range(i, j+1):
        for t in range(i, j+1):
            if q == t:
                J[q][t] = c
            if q < t:
                J[q][t] = -1 * s
            if q > t:
                J[q][t] = s
    return J


def givens_rotation(A,i,j):
    # G = np.copy(A)
    G = np.zeros(shape=(len(A), len(A)))
    sign = np.sign(A[j][j])
    if sign == 0:
        sign = 1
    c = (sign * A[j][j]) / math.sqrt(A[j][j] ** 2 + A[i][j] ** 2)
    s = ((-1 * sign) * A[i][j]) / math.sqrt(A[j][j] ** 2 + A[i][j] ** 2)
    for q in range(0, len(G)):
        for t in range(0, len(G[0])):
            # if A[q][t] != 0:
                if q == i and q == t or (q == j and q == t):
                    G[q][t] = c
                elif q == t:
                    G[q][t] = 1
                elif q == i and t == j:
                    G[q][t] = s
                elif q == j and t == i:
                    G[q][t] = -1*s
                else:
                    G[q][t] = 0

    return G

def qr_decompose(A):
    length = len(A)
    Q = np.identity(length)
    R = np.copy(A)

    for k in range (0, min(len(A[0]), length-1)):
        for j in range(k+1, length):
            J = givens_rotation(R,j,k)
            R = np.matmul(J, R)
            Q = np.matmul(Q, np.transpose(J))
    return (Q,R)


def backward_substitute(R, y):
    x = np.array(y)
    n = len(y)
    for j in range(n-1,-1, -1):
        x[j] = y[j]
        for i in range(j+1, n):
            x[j] -= (R[j, i] * x[i])
        x[j] = x[j] / R[j, j]
    return x

def linsolve(A, *bs):
    """Return (x1, ..., xn), such that A @ xk = bs[k]"""
    Q, R = qr_decompose(A)
    res = []
    for b in bs:
        z = np.matmul(np.transpose(Q), b)
        backward = backward_substitute(R, z)
        res.append(backward)
    return tuple(res)


def main():

  if __name__ == "__main__": main()