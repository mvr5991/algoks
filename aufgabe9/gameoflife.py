#!/usr/bin/env python3

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.animation as animation


glider = np.array([[0,1,0],
                   [0,0,1],
                   [1,1,1]],
                  dtype=bool)


c10orthogonal = np.array([[0,1,1,0,0,1,1,0],
                          [0,0,0,1,1,0,0,0],
                          [0,0,0,1,1,0,0,0],
                          [1,0,1,0,0,1,0,1],
                          [1,0,0,0,0,0,0,1],
                          [0,0,0,0,0,0,0,0],
                          [1,0,0,0,0,0,0,1],
                          [0,1,1,0,0,1,1,0],
                          [0,0,1,1,1,1,0,0],
                          [0,0,0,0,0,0,0,0],
                          [0,0,0,1,1,0,0,0],
                          [0,0,0,1,1,0,0,0]],
                         dtype=bool)


def gamegrid(w, h, entities):
    grid = np.zeros((h,w), dtype=bool)
    for (entity, x, y) in entities:
        add_entity(grid, entity, x, y)
    return grid


def add_entity(grid, entity, y, x):
    for k in range(len(entity)):
        for j in range(len(entity[0])):
            grid[y+k][x+j] = entity[k][j]

    return grid


def next_step(grid):
    futureGen = np.copy(grid)
    for y in range(len(grid)):
        for x in range(len(grid[0])):
            neighbours = aliveNeighbours(grid,y,x)
            print(neighbours)

            #apply rules
            if grid[y][x] == 0 and neighbours == 3:
                futureGen[y][x] = 1
            elif grid[y][x] == 1 and neighbours < 2:
                futureGen[y][x] = 0
            elif grid[y][x] == 1 and 2 <= neighbours < 4:
                futureGen[y][x] = 1
            elif grid[y][x] == 1 and neighbours > 3:
                futureGen[y][x] = 0
    return futureGen


def aliveNeighbours(grid, y, x):
    ret = 0
    for j in range(-1, 2):
        for k in range(-1, 2):
            yCoord = j+y
            xCoord = k+x
            if j == 0 and k == 0:
                continue
            if yCoord >= len(grid):
                yCoord = 0
            elif yCoord < 0:
                yCoord = len(grid)-1
            if xCoord >= len(grid[0]):
                xCoord = 0
            elif xCoord < 0:
                xCoord = len(grid[0]) - 1
            if grid[yCoord][xCoord] == 1:
                ret += 1
    return ret


def gameoflife(grid, steps=100):
    fig, ax = plt.subplots()
    mat = ax.matshow(grid, cmap=cm.gray_r)
    ani = animation.FuncAnimation(fig, lambda _: mat.set_data(next_step(grid)),
                                  frames=100,
                                  interval=50,
                                  blit=False)
    plt.show()


def main():
    grid = gamegrid(40, 40, [(glider, 13, 4),
                             (c10orthogonal, 25, 25)])
    # array = np.array([[1, 0, 1, 0], [1, 0, 1, 0], [1, 0, 0, 0], [0, 0, 0, 0]], dtype=bool)
    # next_step(array)
    gameoflife(grid)

if __name__ == "__main__": main()