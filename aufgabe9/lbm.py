#!/usr/bin/env python3

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.animation as animation

weights = np.array([1./36, 1./9, 1./36,
                    1./9,  4./9, 1./9,
                    1./36, 1./9, 1./36])

directions = np.array([(-1,-1), ( 0,-1), (1,-1),
                       (-1, 0), ( 0, 0), (1, 0),
                       (-1, 1), ( 0, 1), (1, 1)])

inverse_dir = np.array([8, 7, 6, 5, 4, 3, 2, 1, 0])

def density(f):
    x = np.zeros(f[0].shape)
    for i in f:
        x[:] += i[:]
    return x


def velocity(f):
    ux = 1/density(f)*density(f[:]*np.reshape(directions[:,0], (9,1,1)))
    uy = 1/density(f)*density(f[:]*np.reshape(directions[:,1], (9,1,1)))
    return (ux, uy)


def f_eq(f):
    x=np.zeros(f.shape)
    u = velocity(f)
    for i in range(9):
        x[i, :, :]=weights[i]*density(f)*(1.+3.*(directions[i,0]*u[0]+directions[i, 1]*u[1])+9./2*(directions[i,0]*u[0]+directions[i,1]*u[1])**2-3./2*(u[0]**2+u[1]**2))
    return x


def collide(f, omega):
    f[:]-=omega*(f[:]-f_eq(f)[:])
    return f


def stream(f):
    x = f.copy()
    for i in range(9)
    temp = np.roll(np.roll(x[i,:,:], directions[i,0],0), directions[i,1],1)
    f[i,1_f.shape[1]-1, 1:f.shape[2]-1] = temp[1:f.shape[1]-1,1:f.shape[2]-1]
    return f


def noslip(f, masklist):
    x = f.copy()
    for i in masklist:
        f[:,i[0], i[1]] = x[:, i[0], i[1]][::-1]


flow = np.array([1./36, 1./9, 1.6/36,
                 1./9,  4./9, 1.6/9,
                 1./36, 1./9, 1.6/36])


def lbm(W, H, timesteps=1000, omega=1.85):
    fig, ax = plt.subplots()
    f = weights.reshape((9,1,1)) * np.ones((9, W, H))
    f[:, 0, :] = flow.reshape(9,1)
    # create the list of obstacles
    mask = np.zeros((W,H),dtype=bool)
    mask[int(0.2*W):int(0.22*W), int(0.4*H):int(0.6*H)] = 1
    mask[1:-1, 1] = 1
    mask[1:-1,-2] = 1
    masklist = np.argwhere(mask)

    (ux, uy) = velocity(f)
    mat = ax.matshow((np.sqrt(ux*ux + uy*uy)).transpose(), cmap=cm.GnBu)
    def update_velocity_field():
        nonlocal f
        for rep in range(10):
            # inflow
            f[:, 0, :] = flow.reshape(9,1)
            # copying outflow
            f[:, -2, :] = f[:, -1, :]
            f = stream(noslip(collide(f, omega), masklist))
        (ux, uy) = velocity(f)
        mat.set_data((np.sqrt(ux*ux + uy*uy)).transpose())
    ani = animation.FuncAnimation(fig, lambda _: update_velocity_field(),
                                  frames=10000,
                                  interval=10,
                                  blit=False)

    plt.show()


def main():
    lbm(300, 60)


if __name__ == "__main__": main()